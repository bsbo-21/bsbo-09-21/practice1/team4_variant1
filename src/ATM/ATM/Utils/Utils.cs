﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Utils
{
    public static class Utils
    {
        public static T SelectCommandFromEnum<T>()
        where T : struct
        {
            Enum.GetNames(typeof(T)).ToList().ForEach(Console.WriteLine);
            Console.Write("Enter a command: ");
            return Enum.Parse<T>(Console.ReadLine() ?? string.Empty);
        }
    }
}
