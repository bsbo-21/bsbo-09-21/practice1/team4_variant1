﻿namespace ATM.AtmModules
{
    public class CashVault
    {
        public decimal MaximalCashRoom { get; }
        public decimal Cash { get; private set; } = 0.0M;
        public CashVault(decimal maximalCashRoom) => MaximalCashRoom = maximalCashRoom;

        public bool IsEnoughtCash(decimal cash) => (Cash >= cash) && (cash > 0);
        public bool IsEnoughtRoom(decimal cash) => (MaximalCashRoom - Cash >= cash) && (cash > 0);
        public bool IsValidCash(decimal cash) => (MaximalCashRoom >= cash) && (cash > 0);
        public void ResetCashVault(decimal cash)
        {
            if (!IsValidCash(cash))
                throw new InvalidDataException("Too many cash for reset vault");

            Cash = cash;
        }

        public void AcceptCashFromClient(decimal cash)
        {
            if(!IsEnoughtRoom(cash))
                throw new InvalidOperationException("No enought room in vault");

            Cash += cash;
        }
        public void GiveCashToClient(decimal cash)
        {
            if (!IsEnoughtCash(cash))
                throw new InvalidOperationException("No enought cash in vault");

            Cash -= cash;
        }
    }
}
