﻿namespace ATM.AtmModules
{
    class ATMFirmware
    {
        public abstract class AtmState
        {
            public CashVault CashVault { get; }
            public abstract void ProcessAction();
            protected AtmState(CashVault cashVault) => CashVault = cashVault;
        }

        class AtmIdleState : AtmState
        {
            public event Action<BankApi.CollectorBankApi.CollectorAccessToken>? CollectorAuthenticated = null;
            public event Action<BankApi.ClientBankApi.ClientAccessToken>? ClientAuthenticated = null;
            public AtmIdleState(CashVault cashVault) : base(cashVault) { }

            private enum Command
            {
                AuthenticateCollector,
                AuthenticateClient
            }
            public override void ProcessAction()
            {
                try
                {
                    switch (Utils.Utils.SelectCommandFromEnum<Command>())
                    {
                        case Command.AuthenticateCollector:
                            Console.WriteLine("[Collector] -> [AtmIdleState].[AuthenticateCollector]");
                            AuthenticateCollector();
                            break;
                        case Command.AuthenticateClient:
                            Console.WriteLine("[Client] -> [AtmIdleState].[AuthenticateClient]");
                            AuthenticateClient();
                            break;
                    }
                }
                catch (Exception error)
                {
                    Console.Error.WriteLine(error.Message);
                }
            }

            private void AuthenticateCollector()
            {
                Console.WriteLine("Collector authentication");
                Console.Write("Enter a card number: ");
                var card = Console.ReadLine();
                Console.Write("Enter a card pin: ");
                var pin = Console.ReadLine();

                Console.WriteLine("[AtmIdleState] -> [CollectorBankApi].[AuthenticateCollector]");
                var token = BankApi.CollectorBankApi.AuthenticateCollector(new(card), pin);
                Console.WriteLine("Success operation");
                CollectorAuthenticated?.Invoke(token);
            }
            private void AuthenticateClient()
            {
                Console.WriteLine("Client authentication");
                Console.Write("Enter a card number: ");
                var card = Console.ReadLine();
                Console.Write("Enter a card pin: ");
                var pin = Console.ReadLine();

                Console.WriteLine("[AtmIdleState] -> [ClientBankApi].[AuthenticateClient]");
                var token = BankApi.ClientBankApi.AuthenticateClient(new(card), pin);
                Console.WriteLine("Success operation");
                ClientAuthenticated?.Invoke(token);
            }
        }
        class CollectorWorkState : AtmState
        {
            public event Action? Leave = null;
            public CollectorWorkState(CashVault cashVault) : base(cashVault) { }

            private enum Command
            {
                UpdateCash,
                Leave
            }
            public override void ProcessAction()
            {
                try
                {
                    switch (Utils.Utils.SelectCommandFromEnum<Command>())
                    {
                        case Command.UpdateCash:
                            Console.WriteLine("[Collector] -> [CollectorWorkState].[UpdateCash]");
                            UpdateCash();
                            break;
                        case Command.Leave:
                            Console.WriteLine("[Collector] -> [CollectorWorkState].[Leave]");
                            Leave?.Invoke();
                            break;
                    }
                }
                catch (Exception error)
                {
                    Console.Error.WriteLine(error.Message);
                }
            }

            private void UpdateCash()
            {
                Console.Write("Enter a cash: ");
                var cash = decimal.Parse(Console.ReadLine() ?? string.Empty);

                Console.WriteLine("[CollectorWorkState] -> [CashVault].[IsValidCash]");
                if (CashVault.IsValidCash(cash))
                {
                    Console.WriteLine("[CollectorWorkState] -> [CashVault].[ResetCashVault]");
                    CashVault.ResetCashVault(cash);
                    Console.WriteLine("Success operation");
                }
                else
                {
                    Console.WriteLine("Invalid sum for current cash vault");
                }
            }
        }
        class ClientWorkState : AtmState
        {
            public event Action? Leave = null;
            public BankApi.ClientBankApi.ClientAccessToken Token { get; }
            public ClientWorkState(CashVault cashVault, BankApi.ClientBankApi.ClientAccessToken token)
            : base(cashVault) => Token = token;

            private enum Command
            {
                WithdrawCash,
                DepositCash,
                CheckCash,
                Leave
            }
            public override void ProcessAction()
            {
                try
                {
                    switch (Utils.Utils.SelectCommandFromEnum<Command>())
                    {
                        case Command.WithdrawCash:
                            Console.WriteLine("[Client] -> [ClientWorkState].[ConfirmWithdraw]");
                            WithdrawCash();
                            break;
                        case Command.DepositCash:
                            Console.WriteLine("[Client] -> [ClientWorkState].[ConfirmDeposit]");
                            DepositCash();
                            break;
                        case Command.CheckCash:
                            Console.WriteLine("[Client] -> [ClientWorkState].[CheckCash]");
                            CheckCash();
                            break;
                        case Command.Leave:
                            Console.WriteLine("[Client] -> [ClientWorkState].[Leave]");
                            Leave?.Invoke();
                            break;
                    }
                }
                catch (Exception error)
                {
                    Console.Error.WriteLine(error.Message);
                }
            }
            private void WithdrawCash()
            {
                Console.Write("Enter a cash: ");
                var cash = decimal.Parse(Console.ReadLine());

                Console.WriteLine("[ClientWorkState] -> [CashVault].[IsEnoughtCash]");
                if (CashVault.IsEnoughtCash(cash))
                {
                    try
                    {
                        Console.WriteLine("[ClientWorkState] -> [ClientBankApi].[ConfirmWithdraw]");
                        BankApi.ClientBankApi.ConfirmWithdraw(Token, cash);
                        Console.WriteLine("[ClientWorkState] -> [CashVault].[GiveCashToClient]");
                        CashVault.GiveCashToClient(cash);
                        Console.WriteLine($"Accept cash {cash}");
                    }
                    catch(Exception error)
                    {
                        Console.Error.WriteLine(error.Message);
                    }
                }
                else
                {
                    Console.Error.WriteLine("No enought cash in ATM");
                }
            }
            private void DepositCash()
            {
                Console.Write("Enter a cash: ");
                var cash = decimal.Parse(Console.ReadLine());

                Console.WriteLine("[ClientWorkState] -> [CashVault].[IsEnoughtRoom]");
                if (CashVault.IsEnoughtRoom(cash))
                {
                    try
                    {
                        Console.WriteLine("[ClientWorkState] -> [ClientBankApi].[ConfirmDeposit]");
                        BankApi.ClientBankApi.ConfirmDeposit(Token, cash);
                        Console.WriteLine("[ClientWorkState] -> [CashVault].[AcceptCashFromClient]");
                        CashVault.AcceptCashFromClient(cash);
                        Console.WriteLine($"Accept cash {cash}");
                    }
                    catch(Exception error)
                    {
                        Console.Error.WriteLine(error.Message);
                    }
                }
                else
                {
                    Console.Error.WriteLine("No enought room in ATM");
                }
            }
            private void CheckCash()
            {
                Console.WriteLine($"Your cash: { BankApi.ClientBankApi.CheckCash(Token) }");
            }
        }

       

        private AtmState? Current { get; set; } = null;
        public CashVault CashVault { get; } = new(50000);

        public ATMFirmware()
        {
            ExitToIdleState();
        }
        public void Execute()
        {
            while (true)
                Current?.ProcessAction();
        }


        private void ExitToIdleState()
        {
            var state = new AtmIdleState(CashVault);
            state.CollectorAuthenticated += OnCollectorAuthenticated;
            state.ClientAuthenticated += OnClientAuthenticated;
            Current = state;
        }
        public void OnCollectorAuthenticated(BankApi.CollectorBankApi.CollectorAccessToken token)
        {
            var state = new CollectorWorkState(CashVault);
            state.Leave += ExitToIdleState;
            Current = state;
        }
        public void OnClientAuthenticated(BankApi.ClientBankApi.ClientAccessToken token)
        {
            var state = new ClientWorkState(CashVault, token);
            state.Leave += ExitToIdleState;
            Current = state;
        }
        

        /*
        public void OnCollectorCardTouched(CollectorBankApi.CollectorCard card)
        {
            var state = new AuthorizeCollectorCard(Display, CardReader, CashVault, Dispenser, card);
            state.SuccessAuthorization += OnCollectorCardAuthorized;
            state.FailedAuthorization += ExitToIdleState;
            Current = state;
        }
        public void OnCollectorCardAuthorized(CollectorBankApi.CollectorAccessToken token)
        {
            var state = new IdleCollectorWorkState(Display, CardReader, CashVault, Dispenser, token);
            state.Leave += ExitToIdleState;
            Current = state;
        }
        
        public void OnClientCardTouched(ClientBankApi.ClientCard card)
        {
            var state = new AuthorizeClientCard(Display, CardReader, CashVault, Dispenser, card);
            state.SuccessAuthorization += OnClientCardAuthorized;
            state.FailedAuthorization += ExitToIdleState;
            Current = state;
        }
        public void OnClientCardAuthorized(ClientBankApi.ClientAccessToken token)
        {
            var state = new IdleClientWorkState(Display, CardReader, CashVault, Dispenser, token);
            state.CheckBalance += OnClientCheckBalance;
            state.ConfirmWithdraw += OnClientWithdrawCash;
            state.ConfirmDeposit += OnClientDepositCash;
            state.ClientLeave += ExitToIdleState;
            Current = state;
        }
        public void OnClientDepositCash(ClientBankApi.ClientAccessToken token)
        {
            var state = new ClientDepositCashState(Display, CardReader, CashVault, Dispenser, token);
            state.ClientLeave += OnClientCardAuthorized;
            Current = state;
        }
        public void OnClientWithdrawCash(ClientBankApi.ClientAccessToken token)
        {
            var state = new ClientWithdrawCashState(Display, CardReader, CashVault, Dispenser, token);
            state.ClientLeave += OnClientCardAuthorized;
            Current = state;
        }
        public void OnClientCheckBalance(ClientBankApi.ClientAccessToken token)
        {
            var state = new ClientCheckBalanceState(Display, CardReader, CashVault, Dispenser, token);
            state.ClientLeave += OnClientCardAuthorized;
            Current = state;
        }

        */
    }
}