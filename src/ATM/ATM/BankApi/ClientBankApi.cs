﻿namespace ATM.BankApi
{
    public static class ClientBankApi
    {
        public record struct ClientAccessToken(ClientCard Card);
        public record struct ClientCard(string Number);
        private class Client
        {
            public decimal Deposit { get; set; }
            public ClientCard Card { get; }
            public string Pin { get; }
            public Client(ClientCard card, string pin)
            {
                Deposit = 0.0M;
                Card = card;
                Pin = pin;
            }
        }

        private static Dictionary<ClientCard, Client> Clients { get; } = new();
        static ClientBankApi()
        {
            Clients.Add(new("1111222233334444"), new(new("1111222233334444"), "1234"));
            Clients.Add(new("5555666677778888"), new(new("5555666677778888"), "5678"));
        }

        static public ClientAccessToken AuthenticateClient(ClientCard card, string pin)
        {
            if (!Clients.ContainsKey(card))
                throw new InvalidOperationException("Invalid card number");

            if (Clients[card].Pin != pin)
                throw new InvalidOperationException("Invalid card pin");

            return new(card);
        }
        static public void ConfirmWithdraw(ClientAccessToken token, decimal cash)
        {
            var client = Clients?.GetValueOrDefault(token.Card, null) ?? null;
            if (client == null)
                throw new InvalidOperationException("Invalid user token");

            if (client.Deposit < cash)
                throw new InvalidOperationException("No enought cash on user deposit");

            client.Deposit -= cash;
        }
        static public void ConfirmDeposit(ClientAccessToken token, decimal cash)
        {
            var client = Clients?.GetValueOrDefault(token.Card, null) ?? null;
            if (client == null)
                throw new InvalidOperationException("Invalid user token");

            client.Deposit += cash;
        }
        static public decimal CheckCash(ClientAccessToken token)
        {
            return Clients?.GetValueOrDefault(token.Card, null)?.Deposit ?? 0.0M;
        }
    }
}