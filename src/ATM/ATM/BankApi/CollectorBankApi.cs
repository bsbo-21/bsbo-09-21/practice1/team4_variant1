﻿namespace ATM.BankApi
{
    public static class CollectorBankApi
    {
        public record struct CollectorAccessToken(CollectorCard card);
        public record struct CollectorCard(string Number);
        private class Collector
        {
            public CollectorCard Card { get; }
            public string Pin { get; }
            public Collector(CollectorCard card, string pin)
            {
                Card = card;
                Pin = pin;
            }
        }

        private static Dictionary<CollectorCard, Collector> Collectors { get; } = new();
        static CollectorBankApi()
        {
            Collectors.Add(new("11223344"), new(new("11223344"), "1234"));
            Collectors.Add(new("55667788"), new(new("55667788"), "5678"));
        }
        
        static public CollectorAccessToken AuthenticateCollector(CollectorCard card, string pin)
        {
            if (!Collectors.ContainsKey(card))
                throw new InvalidOperationException("Invalid card number");

            if (Collectors[card].Pin != pin)
                throw new InvalidOperationException("Invalid card pin");

            return new(card);
        }
    }
}